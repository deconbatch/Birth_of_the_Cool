* Birth of the Cool.
One of the creative coding works by deconbatch.
[[https://www.deconbatch.com/2020/07/birth-of-cool.html]['Birth of the Cool.' on Deconbatch's Creative Coding : Examples with Code.]]

[[./example01.png][An example image with mixed up formula.]]

** Description of this creative coding.
   - It's a creative coding work made with Processing. It draws some shape with meaningless mixed up formula.
   - It's a derivative work of [[https://www.deconbatch.com/2020/07/sultans-of-swing.html][Processing code example 'Sultans of Swing']]. I tried to draw the tracks of this animation.
   - I used three layers to draw this time.
     - [[./example.00.back.png][back]]
     - [[./example.00.plane.png][plane]]
     - [[./example.00.wire.png][wire]]
     - [[./example.00.total.png][back + plane + wire]]
   - The 'back' layer allows any background.
     - [[./example.10.with.back.png][An example with 'back' layer.]]
     - [[./example.10.without.back.png][An example without 'back' layer.]]
** Change log.
   - created : 2020/08/01
